﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UnityVersion : MonoBehaviour {

	public Text unityVersionText;

	// Use this for initialization
	void Start () {
		unityVersionText.text = Application.unityVersion;
	}
	
}
