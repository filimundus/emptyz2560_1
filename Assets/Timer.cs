﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {

	public Text timerText;
	
	// Update is called once per frame
	void Update () {
		timerText.text = Time.realtimeSinceStartup.ToString("F2", System.Globalization.CultureInfo.InvariantCulture);
	}
}
